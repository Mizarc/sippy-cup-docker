FROM node:boron

COPY ./game-news-feed /src

WORKDIR /src

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]