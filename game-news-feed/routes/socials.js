const express = require('express');
const https = require('https');
const axios = require('axios');
const { response } = require('express');

const router = express.Router();

router.get('/:query', (req, res) => {
    console.log('wagh');
    const options1 = twitterOptions(req.params.query)
    twitter_section = '';

    // Fetch newest tweets from user
    axios({
        url: `https://${options1.hostname}${options1.path}`,
        method: options1.method,
        headers: {
            'Authorization': `Bearer ${options1.bearerkey}`,
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        }
    })
    .then(response => {
        return response.data;
    })
    .then((rsp) => {
        const s = createPage(req.params.query, rsp);
        twitter_section = s;
        //res.write(s);
        //res.end();
    })
    .catch(error => {
        console.error(error);
    });

    // Fetch newest reddit posts
    const options2 = redditOptions(req.params.query)
    axios({
        url: `https://${options2.hostname}${options2.path}`,
        method: options2.method,
        headers: {}
    })
    .then(response => {
        res.writeHead(response.status, {'content-type': 'text/html'});  
        return response.data;
    })
    .then((rsp) => {
        const s = appendPage(req.params.query, rsp, twitter_section);
        res.write(s);
        res.end();
    })
    .catch(error => {
        console.error(error);
    });
});

function twitterOptions(username) {
    const options = {
        hostname: 'api.twitter.com/1.1',
        port: 443,
        path: '/statuses/user_timeline.json?',
        method: 'GET',
        bearerkey: 'AAAAAAAAAAAAAAAAAAAAAJ7%2FHgEAAAAARo0%2Fwu%2FxM42l4GRywod4pHRcXcg%3DD7q7FCjGgyz8MWBKO0yK6wgds9mpDXeap7ge1dWI3Q9GZthQmQ'
    }

    const queries = 'screen_name=' + 'minecraft' +
        '&count=' + '5'

    options.path += queries;
    return options;
}

function redditOptions(username) {
    const options = {
        hostname: 'www.reddit.com/r',
        port: 443,
        path: '/Minecraft/hot/.json?',
        method: 'GET',
        bearerkey: 'AAAAAAAAAAAAAAAAAAAAAJ7%2FHgEAAAAARo0%2Fwu%2FxM42l4GRywod4pHRcXcg%3DD7q7FCjGgyz8MWBKO0yK6wgds9mpDXeap7ge1dWI3Q9GZthQmQ'
    }

    //const queries = 'screen_name=' + 'twitterdev' +
    //    '&count=' + '5'

    //options.path += queries;
    return options;
}

function parseTweetsRsp(rsp) {
    let s = '';
    for (let i=0; i < rsp.length; i++) {
        tweet = rsp[i];

        s += `<li>${tweet.text}</li>`;
    }

    return s;
}

function parseRedditRsp(rsp) {
    let s = '';
    for (let i=0; i < rsp.data.children.length; i++) {
        post = rsp.data.children[i];

        s += `<li>${post.data.title}</li><a href=${post.data.url}>Click here to see post</a>`;
    }
    return s;
}

function createPage(title, rsp) {
    const tweets = parseTweetsRsp(rsp);

    const str = '<!DOCTYPE html>' +
        '<html><head><title>Game Socials</title>' +
        '<meta charset="UTF-8"></head>' +
        '<body>' +
            '<a href="/"><button id="back-button" type="button">Go back</button></div></a>' +
            '<h1>Developer Feed for "' + title + '"</h1>' +
            '<div><ol>' + tweets + '</ol></div>';
    return str;
}

function appendPage(title, rsp, existingHTML) {
    console.log("test");
    const posts = parseRedditRsp(rsp);

    const str = existingHTML +
        '<div><h1>Community Feed for "' + title + '"</h1><ol>' + posts + '<ol></div>' +
        '</body></html>';

    return str;
}

module.exports = router;