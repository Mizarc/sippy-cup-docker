const express = require('express');
const https = require('https');
const axios = require('axios');
const { response } = require('express');

const router = express.Router();

// Fetch all Games
router.get('/:query', (req, res) => {
    console.log('wagh');
    const options = createOptions(req.params.query)

    axios({
        url: `https://${options.hostname}${options.path}`,
        method: options.method,
        headers: {
            'Accept': 'application/json',
            'user-key': '2220b58b5833699af982693c22b6c0d0',
        },
    })
    .then(response => {
        res.writeHead(response.status, {'content-type': 'text/html'});  
        return response.data;
    })
    .then((rsp) => {
        const s = createPage(req.params.query, rsp);
        res.write(s);
        res.end();
    })
    .catch(error => {
        console.error(error);
    });
});

function createOptions(searchQuery) {
    const options = {
        hostname: 'api-v3.igdb.com',
        port: 443,
        path: '/games?',
        method: 'POST'
    }

    const queries = 'search=' + searchQuery +
        '&limit=' + '10' +
        '&fields=' + 'slug,name,websites'

    options.path += queries;
    return options;
}

// Fetch all associated websites for a selected game
function fetchSocials(websiteID, game_slug) {
    const options = createSocialsOptions(websiteID)

    return axios({
        url: `https://${options.hostname}${options.path}`,
        method: options.method,
        headers: {
            'Accept': 'application/json',
            'user-key': '2220b58b5833699af982693c22b6c0d0',
        },
    })
    .then(response => {
        return response.data;
    })
    .then((rsp) => {
        const s = appendSocials(game_slug, rsp);
    })
    .catch(error => {
        console.error(error);
    });
}

function createSocialsOptions(referenceID) {
    const options = {
        hostname: 'api-v3.igdb.com',
        port: 443,
        path: '/websites?',
        method: 'POST'
    }

    const queries = 'id=' + referenceID +
        '&fields=' + 'category,url,game'

    options.path += queries;
    return options;
}

function parseGamesRsp(rsp) {
    let s = '';
    for (let i=0; i < rsp.length; i++) {
        game = rsp[i];
        fetchSocials(game.id, game.slug);

        s += `<input type="checkbox" id="vehicle2" value="Car"> <label for="${game.name}">` + game.name + "</label><br>";
    }

    return s;
}

function createPage(title, rsp) {
    const games = parseGamesRsp(rsp);

    const str = '<!DOCTYPE html>' +
        '<html><head><title>Game Search</title></head>' +
        '<body>' +
            '<h1>Showing results for "' + title + '"</h1>' +
            '<div>' + games + '</div>' +
            '<div><button id="submit-button" type="button">Save</button>' +
            '<a href="/"><button id="back-button" type="button">Go back</button></div></a>' +
        '</body></html>';
    return str;
}

function appendSocials(game_name, rsp) {
    if (rsp.category == 5)
    {
        console.log("twitter");
    }

    if (rsp.category == 14)
    {
        console.log("reddit");
    }
}

module.exports = router;