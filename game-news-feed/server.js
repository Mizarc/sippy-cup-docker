const express = require('express');
const Vue = require('vue');
var path = require('path');
const IGDBRouter = require('./routes/igdb');
const SocialsRouter = require('./routes/socials');

const app = express();

const hostname = '127.0.0.1';
const port = 3000;

app.use(express.static(__dirname + '/client'));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/client/index.html'));
});

app.use('/search?', IGDBRouter)
app.use('/feed?', SocialsRouter)

app.listen(port, function () {
    console.log(`Express app listening at http://${hostname}:${port}/`);
});
